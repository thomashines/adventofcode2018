#!/usr/bin/env python3

import sys

polymer = list("".join(line.strip() for line in sys.stdin))

def peak(l):
    if len(l) == 0:
        return None
    return l[-1]

def pop(l):
    if len(l) == 0:
        return None
    return l.pop(-1)

def push(l, v):
    l.append(v)

diff = ord("a") - ord("A")
def reacts(a, b):
    return abs(ord(a) - ord(b)) == diff

letters = tuple(chr(l) for l in range(ord("a"), ord("z") + 1))
lengths = []
for letter in letters:
    upper = letter.upper()
    collapsed = []

    for unit in polymer:
        if unit == letter or unit == upper:
            continue
        elif peak(collapsed) is not None and reacts(peak(collapsed), unit):
            pop(collapsed)
        else:
            push(collapsed, unit)

    lengths.append(len(collapsed))

print(min(lengths))

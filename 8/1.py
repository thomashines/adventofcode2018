#!/usr/bin/env python3

import sys

input = " ".join(l.strip() for l in sys.stdin)
parts = tuple(int(p) for p in input.strip().split(" "))
print(parts)

def parse(parts, id="A"):
    n_children = parts[0]
    n_metas = parts[1]
    children = []
    remaining = parts[2:]
    for i in range(n_children):
        child, remaining = parse(remaining)
        children.append(child)
    metas = remaining[:n_metas]
    print(metas)
    return {
        "n_children": n_children,
        "n_metas": n_metas,
        "children": children,
        "metas": metas,
    }, remaining[n_metas:]
parsed, remaining = parse(parts)

def sum_meta(tree):
    return sum(tree["metas"]) + sum(sum_meta(c) for c in tree["children"])

print(sum_meta(parsed))

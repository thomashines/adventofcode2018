#!/usr/bin/env python3

import sys

coords = tuple(tuple(int(x) for x in p.strip().split(", ")) for p in sys.stdin)
points = {}
letter = "A"
for c in coords:
    points[letter] = c
    letter = chr(ord(letter) + 1)
print(points)

minx = None
maxx = None
miny = None
maxy = None

for x, y in coords:
    if minx is None or x < minx:
        minx = x
    if maxx is None or x > maxx:
        maxx = x
    if miny is None or y < miny:
        miny = y
    if maxy is None or y > maxy:
        maxy = y

grid = {}

stats = {}

for y in range(miny, maxy + 1):
    grid[y] = {}
    for x in range(minx, maxx + 1):
        closest = None
        dist = None
        tied = None
        for point, coord in points.items():
            this_dist = abs(x - coord[0]) + abs(y - coord[1])
            if closest is None or this_dist < dist:
                closest = point
                dist = this_dist
                tied = False
            elif this_dist == dist:
                tied = True
        if tied:
            grid[y][x] = "."
        else:
            grid[y][x] = closest
            if closest not in stats:
                stats[closest] = {
                    "infinite": False,
                    "count": 0,
                }
            stats[closest]["count"] += 1
            if x == minx or x == maxx or y == miny or y == maxy:
                # On edge
                stats[closest]["infinite"] = True

stringed = ""

for y in sorted(grid.keys()):
    for x in sorted(grid[y].keys()):
        stringed += grid[y][x]
    stringed += "\n"
print(stringed, end="")

max_finite = None
for point, stat in stats.items():
    if not stat["infinite"] and (max_finite is None or stat["count"] > stats[max_finite]["count"]):
        max_finite = point

print(max_finite, stats[max_finite])

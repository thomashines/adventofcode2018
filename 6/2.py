#!/usr/bin/env python3

import sys

coords = tuple(tuple(int(x) for x in p.strip().split(", ")) for p in sys.stdin)
points = {}
letter = "A"
for c in coords:
    points[letter] = c
    letter = chr(ord(letter) + 1)
print(points)

minx = None
maxx = None
miny = None
maxy = None

for x, y in coords:
    if minx is None or x < minx:
        minx = x
    if maxx is None or x > maxx:
        maxx = x
    if miny is None or y < miny:
        miny = y
    if maxy is None or y > maxy:
        maxy = y

grid = {}

safe_size = 0

for y in range(miny, maxy + 1):
    grid[y] = {}
    for x in range(minx, maxx + 1):
        total_dist = 0
        for point, coord in points.items():
            total_dist += abs(x - coord[0]) + abs(y - coord[1])
        if total_dist < 10000:
            grid[y][x] = "#"
            safe_size += 1
        else:
            grid[y][x] = "."

stringed = ""

for y in sorted(grid.keys()):
    for x in sorted(grid[y].keys()):
        stringed += grid[y][x]
    stringed += "\n"
print(stringed, end="")

print(safe_size)

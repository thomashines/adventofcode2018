#!/usr/bin/env python3

import sys
import time

lookup = {
    0: ".",
    1: "|",
    2: "#",
}
reverse_lookup = {
    ".": 0,
    "|": 1,
    "#": 2,
}

inputs = tuple(l.strip() for l in sys.stdin)

height = len(inputs)
width = len(inputs[0])
grid = []
for y in range(height):
    for x in range(width):
        grid.append(reverse_lookup[inputs[y][x]])
grid = tuple(grid)

def get_adjacents(grid, x, y, height, width):
    for dx in (-1, 0, +1):
        for dy in (-1, 0, +1):
            if dx != 0 or dy != 0:
                a_x = x + dx
                a_y = y + dy
                if a_x >= 0 and a_x < width and a_y >= 0 and a_y < height:
                    yield grid[width * (y + dy) + x + dx]

def value(grid):
    trees = 0
    lumberyards = 0
    for thing in grid:
        if thing == 1:
            trees += 1
        elif thing == 2:
            lumberyards += 1
    return trees * lumberyards

def print_grid(grid, width, height):
    for y in range(height):
        line = ""
        for x in range(width):
            line += lookup[grid[width * y + x]]
        print(line)
    # print("value", value(grid))

print_grid(grid, width, height)

past_grids = {}

sequence = []

for m in range(1, 1000000000):
    if grid in past_grids:
        grid = past_grids[grid]
        if grid in sequence:
            # After minute m, we are at this_index
            this_index = sequence.index(grid)
            # print(m, len(sequence), value(grid), this_index)
            # Add remaining minutes, mod length and you're good
            final_index = (this_index + (1000000000 - m)) % len(sequence)
            # print("final", value(sequence[final_index]))
            # break
        else:
            sequence.append(grid)
    else:
        new_grid = list(grid)
        for y in range(height):
            for x in range(width):
                thing = grid[width * y + x]
                if thing == 0:
                    # open
                    trees = 0
                    for adjacent in get_adjacents(grid, x, y, height, width):
                        if adjacent == 1:
                            trees += 1
                    if trees >= 3:
                        new_grid[width * y + x] = 1
                    else:
                        new_grid[width * y + x] = 0
                elif thing == 1:
                    # open
                    lumberyards = 0
                    for adjacent in get_adjacents(grid, x, y, height, width):
                        if adjacent == 2:
                            lumberyards += 1
                    if lumberyards >= 3:
                        new_grid[width * y + x] = 2
                    else:
                        new_grid[width * y + x] = 1
                elif thing == 2:
                    # open
                    trees = 0
                    lumberyards = 0
                    for adjacent in get_adjacents(grid, x, y, height, width):
                        if adjacent == 1:
                            trees += 1
                        elif adjacent == 2:
                            lumberyards += 1
                    if trees >= 1 and lumberyards >= 1:
                        new_grid[width * y + x] = 2
                    else:
                        new_grid[width * y + x] = 0
        past_grids[grid] = tuple(new_grid)
        grid = past_grids[grid]
    print_grid(grid, width, height)
    time.sleep(0.1)
    # print("after minute", m)
    # if m == 10:
    #     print("after minute", m)
    #     print_grid(grid, width, height)
    #     print("value", value(grid))

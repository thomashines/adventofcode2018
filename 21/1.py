#!/usr/bin/env python3

import sys
import random
import time

ops = {
    "divi": lambda m, a, b: m[a] // b,
    "addr": lambda m, a, b: m[a] + m[b],
    "addi": lambda m, a, b: m[a] + b,
    "mulr": lambda m, a, b: m[a] * m[b],
    "muli": lambda m, a, b: m[a] * b,
    "banr": lambda m, a, b: m[a] & m[b],
    "bani": lambda m, a, b: m[a] & b,
    "borr": lambda m, a, b: m[a] | m[b],
    "bori": lambda m, a, b: m[a] | b,
    "setr": lambda m, a, b: m[a],
    "seti": lambda m, a, b: a,
    "gtir": lambda m, a, b: 1 if a > m[b] else 0,
    "gtri": lambda m, a, b: 1 if m[a] > b else 0,
    "gtrr": lambda m, a, b: 1 if m[a] > m[b] else 0,
    "eqir": lambda m, a, b: 1 if a == m[b] else 0,
    "eqri": lambda m, a, b: 1 if m[a] == b else 0,
    "eqrr": lambda m, a, b: 1 if m[a] == m[b] else 0,
}

def do(m, op, a, b, c):
    m2 = list(m)
    m2[c] = ops[op](m2, a, b)
    return m2

inputs = tuple(l.strip() for l in sys.stdin)

ip = int(inputs[0][4:])
print("ip", ip)

program = tuple(tuple(input.split(" ")) for input in inputs[1:])
program = tuple(tuple((i[0],) + tuple(int(n) for n in i[1:])) for i in program)
print("program", program)

m2s = set()
m20 = None
m = [0, 0, 0, 0, 0, 0]
while m[ip] >= 0 and m[ip] < len(program):
    # Check state before test A == C that exits
    if program[m[ip]] == ("eqrr", 2, 0, 3):
        if m20 is None:
            m20 = m[2]
        if m[2] not in m2s:
            m2s.add(m[2])
            print(len(m2s), m20, m[2])
    if program[m[ip]][0] != "nop":
        m = do(m, *program[m[ip]])
    m[ip] += 1

# [
#   0: A,
#   1: B,
#   2: C,
#   3: D,
#   4: IP,
#   5: E,
# ]

#ip 4
# This does nothing
#  0: seti 123 0 2      # C = 123
#  1: bani 2 456 2      # C = C & 456 = 72
#  2: eqri 2 72 2       # C = C == 72 = 1
#  3: addr 2 4 4        # IP = C + IP = 1 + 3 + 1 = 5, goto 5
#  4: seti 0 0 4        # IP = 0 = 0 + 1 = 1, goto 1
#  5: seti 0 8 2        # C = 0

#  6: bori 2 65536 5    # E = C | 65536 = 65536
#  7: seti 2238642 0 2  # C = 2238642
#  8: bani 5 255 3      # D = E & 255 = 65536 & 255 = 0
#  9: addr 2 3 2        # C = C + D = 2238642
# 10: bani 2 16777215 2 # C = C & 16777215 = 2238642 & 16777215 = 2238642
# 11: muli 2 65899 2    # C = C * 65899 = 2238642 * 65899 = 147524269158
# 12: bani 2 16777215 2 # C = C & 16777215 = 147524269158 & 16777215 = 2208870
# 13: gtir 256 5 3      # D = 256 > E = 256 > 65536 = 0
# 14: addr 3 4 4        # IP = D + IP = 0 + 14 + 1 = 15, goto 15
# 15: addi 4 1 4        # IP = IP + 1 = 15 + 1 + 1 = 17, goto 17
# 16: seti 27 3 4       # IP = 27 = 27 + 1 = 28, goto 28
# 17: seti 0 8 3        # D = 0

# This repeats
# 18: addi 3 1 1        # B = D + 1 = 1
# 19: muli 1 256 1      # B = B * 256 = 256
# 20: gtrr 1 5 1        # B = B > E = 256 > 65536 = 0
# 21: addr 1 4 4        # IP = B + IP = 0 + 21 + 1 = 22, goto 22
# 22: addi 4 1 4        # IP = IP + 1 = 22 + 1 + 1 = 24, goto 24
# 23: seti 25 4 4       # IP = 25 + 1 = 26, goto 26
# 24: addi 3 1 3        # D = D + 1 = 1
# 25: seti 17 2 4       # IP = 17 + 1 = 18, goto 18

# 26: setr 3 9 5        # E = D
# 27: seti 7 9 4        # IP = 7 = 7 + 1 = 8, goto 8
# 28: eqrr 2 0 3        # D = C == A
# 29: addr 3 4 4        # IP = D + IP (exit if C == A)
# 30: seti 5 0 4        # IP = 5 = 5 + 1, goto 6

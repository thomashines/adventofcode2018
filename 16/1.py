#!/usr/bin/env python3

import sys

inputs = (l.strip() for l in sys.stdin)

cases = []
program = []

case = None

for input in inputs:
    if input.startswith("Before"):
        case = [list(int(c) for c in input[9:-1].split(", "))]
    elif input.startswith("After"):
        case.append(list(int(c) for c in input[9:-1].split(", ")))
        cases.append(case)
        case = None
    elif case is not None and len(input) > 0:
        case.append(list(int(c) for c in input.split(" ")))
    elif len(input) > 0:
        program.append(list(int(c) for c in input.split(" ")))

def do(m, op, a, b, c):
    m2 = list(m)
    m2[c] = op(m2, a, b)
    return m2

ops = {
    "addr": lambda m, a, b: m[a] + m[b],
    "addi": lambda m, a, b: m[a] + b,
    "mulr": lambda m, a, b: m[a] * m[b],
    "muli": lambda m, a, b: m[a] * b,
    "banr": lambda m, a, b: m[a] & m[b],
    "bani": lambda m, a, b: m[a] & b,
    "borr": lambda m, a, b: m[a] | m[b],
    "bori": lambda m, a, b: m[a] | b,
    "setr": lambda m, a, b: m[a],
    "seti": lambda m, a, b: a,
    "gtir": lambda m, a, b: 1 if a > m[b] else 0,
    "gtri": lambda m, a, b: 1 if m[a] > b else 0,
    "gtrr": lambda m, a, b: 1 if m[a] > m[b] else 0,
    "eqir": lambda m, a, b: 1 if a == m[b] else 0,
    "eqri": lambda m, a, b: 1 if m[a] == b else 0,
    "eqrr": lambda m, a, b: 1 if m[a] == m[b] else 0,
}

preliminary = {}

matches = []
for case in cases:
    matching = set()
    op_code = case[1][0]
    for op_name, op in ops.items():
        if case[2] == do(case[0], op, *case[1][1:]):
            matching.add(op_name)
    if op_code not in preliminary:
        preliminary[op_code] = matching
    else:
        preliminary[op_code] = preliminary[op_code].intersection(matching)
    if len(matching) == 0:
        print("unmatched", case)
    matches.append((case, matching))

print("3 or more", len(tuple(m for m in matches if len(m[1]) >= 3)))

done = False
while not done:
    done = True
    for op_code, prelim_op_names in preliminary.items():
        if len(prelim_op_names) == 1:
            (op_name,) = prelim_op_names
            for other_op_code, other_prelim_op_names in preliminary.items():
                if other_op_code != op_code and op_name in other_prelim_op_names:
                    other_prelim_op_names.remove(op_name)
        elif len(prelim_op_names) > 1:
            done = False

op_codes = {}

for op_code, prelim_op_names in preliminary.items():
    (op_name,) = prelim_op_names
    op_codes[op_code] = op_name

print(op_codes)

m = [0, 0, 0, 0]
for instruction in program:
    m = do(m, ops[op_codes[instruction[0]]], *instruction[1:])

print(m)

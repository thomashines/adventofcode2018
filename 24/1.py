#!/usr/bin/env python3

import re
import sys

input = tuple(l.strip() for l in sys.stdin)

immunes = set()
infections = set()
all = set()

reading = None

# parser = re.compile("^(\d+) units? each with (\d+) hit points \((.+)\) with an attack that does (\d+) (\w+) damage at initiative (\d+)$")

id = 0

for line in input:
    # print(line)
    if line.endswith(":"):
        reading = line[:-1]
        # print(reading)
    elif len(line) > 0:
        # print(line)
        # parsed = parser.match(line)
        # print(parsed.groups())
        immunities = set()
        weaknesses = set()
        armours = re.search("\((.+)\)", line)
        if armours:
            for armour in armours.group(1).split("; "):
                effect, types = armour.split(" to ")
                for type in types.split(", "):
                    if effect == "immune":
                        immunities.add(type)
                    elif effect == "weak":
                        weaknesses.add(type)
                    else:
                        print(armour)
        group = tuple(sorted({
            "attack": re.search("attack that does \d+ (\w+) damage", line).group(1),
            "damage": int(re.search("attack that does (\d+)", line).group(1)),
            "hp": int(re.search("(\d+) hit points", line).group(1)),
            "id": id,
            "immunities": tuple(sorted(immunities)),
            "initiative": int(re.search("initiative (\d+)", line).group(1)),
            "type": reading,
            "units": int(re.search("(\d+) units", line).group(1)),
            "weaknesses": tuple(sorted(weaknesses)),
        }.items()))
        all.add(group)
        if reading == "Immune System":
            immunes.add(group)
        elif reading == "Infection":
            infections.add(group)
        id += 1

# print(immunes)
# print(infections)

def resolve(groups, immune_boost=0):
    all_groups = []
    for group in groups:
        group = dict(group)
        all_groups.append(dict(group))

    all_groups = sorted(all_groups, key=lambda g: g["id"])

    immune = 1
    infection = 1
    for group in all_groups:
        if group["type"] == "Immune System":
            group["damage"] += immune_boost
            group["num"] = immune
            immune += 1
        elif group["type"] == "Infection":
            group["num"] = infection
            infection += 1

    fight = 1

    while len(all_groups) > 0:
        # print("Before fight", fight)
        # all_groups = sorted(all_groups, key=lambda g: g["id"])
        # for type in ("Immune System", "Infection"):
        #     print(type + ":")
        #     for group in all_groups:
        #         if group["type"] == type:
        #             print("Group", group["num"], "contains", len(group["units"]), "units")
        #
        # print()

        # Effective power
        for group in all_groups:
            group["power"] = group["units"] * group["damage"]

        # Target selection
        all_groups = sorted(all_groups, key=lambda g: (-g["power"], -g["initiative"]))
        targeted = set()
        for group in all_groups:
            target = None
            target_damage = None
            for other_group in all_groups:
                if other_group["id"] != group["id"] and other_group["type"] != group["type"] and other_group["id"] not in targeted:
                    damage = group["power"]
                    # print("base", damage)
                    if group["attack"] in other_group["immunities"]:
                        # print("immune")
                        damage = 0
                    if group["attack"] in other_group["weaknesses"]:
                        # print("weak")
                        damage *= 2
                    if damage == 0:
                        # Don't pick a target it can't attack
                        continue
                    # print(group["type"], "group", group["num"], "(power", group["power"], "initiative", group["initiative"], ") would deal defending group", other_group["num"], damage, "damage")
                    # print(group["type"], "group", group["num"], "would deal defending group", other_group["num"], damage, "damage")
                    if target is None or damage > target_damage or (damage == target_damage and other_group["power"] > target["power"]) or (damage == target_damage and other_group["power"] == target["power"] and other_group["initiative"] > target["initiative"]):
                        target = other_group
                        target_damage = damage
            group["target"] = target
            if target is not None:
                # print(group["id"], group["type"], "targets", target["id"])
                targeted.add(target["id"])

        # print()

        # Attacking
        all_groups = sorted(all_groups, key=lambda g: -g["initiative"])
        for group in all_groups:
            group["power"] = group["units"] * group["damage"]
            target = group["target"]
            if target is not None:
                damage = group["power"]
                if group["attack"] in target["immunities"]:
                    damage = 0
                if group["attack"] in target["weaknesses"]:
                    damage *= 2
                # print("group", group)
                # print("target", target)
                # print("damage", damage)
                killed_units = damage // target["hp"]
                # print(group["type"], "group", group["num"], "attacks defending group", target["num"], "killing", killed_units, "units")
                target["units"] = max(0, target["units"] - killed_units)

        # Filter to living units
        all_groups = [g for g in all_groups if g["units"] > 0]

        # print()
        # print("After")

        # Check if game over
        immunes = []
        infections = []
        for group in all_groups:
            group["power"] = group["units"] * group["damage"]
            if group["type"] == "Immune System":
                immunes.append(group["units"])
            elif group["type"] == "Infection":
                infections.append(group["units"])
        # print("immunes", immunes, "infections", infections)
        # print("immunes", sum(immunes), "infections", sum(infections))
        if len(immunes) == 0 or len(infections) == 0:
            return (sum(immunes), sum(infections))

        fight += 1
        if fight > 10000:
            print("calling it off after", fight - 1, "fights")
            return (sum(immunes), sum(infections))
        # print()

immune_boost = 0
while True:
    immunes, infections = resolve(all, immune_boost)
    print(immune_boost, (immunes, infections))
    if infections == 0:
        break
    immune_boost += 1

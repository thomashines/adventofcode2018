#include "inttypes.h"
#include "limits.h"
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#define SIZE 300

int value(int serial, int x, int y) {
  return (((((x + 10) * y + serial) * (x + 10)) / 100) % 10) - 5;
}

int sum_vert(int **rows, int r, int c) {
  return rows[r - 1][c] + rows[r][c] + rows[r + 1][c];
}

int sum_hor(int **rows, int r, int c) {
  return rows[r][c - 1] + rows[r][c] + rows[r][c - 1];
}

int solve(int serial) {
  int *grid = malloc((SIZE + 2) * (SIZE + 2) * sizeof(int));
  memset(grid, 0, (SIZE + 2) * (SIZE + 2) * sizeof(int));
  int **rows = malloc((SIZE + 2) * sizeof(int *));
  for (int r = 0; r <= SIZE + 1; r++) {
    rows[r] = &grid[r * SIZE];
  }

  for (int r = 1; r <= SIZE; r++) {
    for (int c = 1; c <= SIZE; c++) {
      int cell_value = value(serial, c, r);
      rows[r - 1][c - 1] += cell_value;
      rows[r - 1][c] += cell_value;
      rows[r - 1][c + 1] += cell_value;
      rows[r][c - 1] += cell_value;
      rows[r][c] += cell_value;
      rows[r][c + 1] += cell_value;
      rows[r + 1][c - 1] += cell_value;
      rows[r + 1][c] += cell_value;
      rows[r + 1][c + 1] += cell_value;
    }
  }

  int max_r = 1;
  int max_c = 1;
  int max_value = rows[1][1];

  for (int r = 1; r <= SIZE; r++) {
    for (int c = 1; c <= SIZE; c++) {
      if (rows[r][c] > max_value) {
        max_r = r;
        max_c = c;
        max_value = rows[r][c];
      }
    }
  }

  printf("%d, %d = %d\n", max_r, max_c, max_value);

  return max_value;
}

int main(int argc, char **argv) {
  printf("value(%d, %d, %d) = %d\n", 57, 122, 79, value(57, 122, 79));
  printf("value(%d, %d, %d) = %d\n", 39, 217, 196, value(39, 217, 196));
  printf("value(%d, %d, %d) = %d\n", 71, 101, 153, value(71, 101, 153));

  size_t length = 0;
  char *line = NULL;
  while (getline(&line, &length, stdin) != -1) {
    char *end;
    int serial = strtol(line, &end, 10);

    printf("solve(%d) = %d\n", serial, solve(serial));

    free(line);
    length = 0;
    line = NULL;
  }
  return 0;
}

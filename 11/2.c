#include "inttypes.h"
#include "limits.h"
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#define SIZE 300

int value(int serial, int x, int y) {
  return (((((x + 10) * y + serial) * (x + 10)) / 100) % 10) - 5;
}

int sum_square(int **rows, int r0, int c0, int s) {
  int sum = 0;
  for (int r = r0; r < r0 + s; r++) {
    for (int c = c0; c < c0 + s; c++) {
      sum += rows[r][c];
    }
  }
  return sum;
}

int max_square(int **rows, int s, int *max_r, int *max_c) {
  *max_r = 0;
  *max_c = 0;
  int max_value = sum_square(rows, 0, 0, s);

  for (int r = 0; r < SIZE - s; r++) {
    for (int c = 0; c < SIZE - s; c++) {
      int this_value = sum_square(rows, r, c, s);
      if (this_value > max_value) {
        *max_r = r;
        *max_c = c;
        max_value = this_value;
      }
    }
  }

  return max_value;
}

int solve(int serial) {
  int *grid = malloc(SIZE * SIZE * sizeof(int));
  memset(grid, 0, SIZE * SIZE * sizeof(int));
  int **rows = malloc(SIZE * sizeof(int *));
  for (int r = 0; r < SIZE; r++) {
    rows[r] = &grid[r * SIZE];
    for (int c = 0; c < SIZE; c++) {
      rows[r][c] = value(serial, c + 1, r + 1);
    }
  }

  int max_r = 0;
  int max_c = 0;
  int max_s = 1;
  int max_value = rows[0][0];

  for (int s = 1; s <= SIZE; s++) {
    printf("s=%d\n", s);
    fflush(stdout);
    int this_max_r, this_max_c;
    int this_max_value = max_square(rows, s, &this_max_r, &this_max_c);
    if (this_max_value > max_value) {
      max_r = this_max_r;
      max_c = this_max_c;
      max_s = s;
      max_value = this_max_value;
    }
    printf("%d, %d [%d] = %d\n", max_r, max_c, max_s, max_value);
  }

  return max_value;
}

int main(int argc, char **argv) {
  printf("value(%d, %d, %d) = %d\n", 57, 122, 79, value(57, 122, 79));
  printf("value(%d, %d, %d) = %d\n", 39, 217, 196, value(39, 217, 196));
  printf("value(%d, %d, %d) = %d\n", 71, 101, 153, value(71, 101, 153));

  size_t length = 0;
  char *line = NULL;
  while (getline(&line, &length, stdin) != -1) {
    char *end;
    int serial = strtol(line, &end, 10);

    printf("solve(%d) = %d\n", serial, solve(serial));

    free(line);
    length = 0;
    line = NULL;
  }
  return 0;
}

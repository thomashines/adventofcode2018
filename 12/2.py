#!/usr/bin/env python3

from PIL import Image
import sys

lines = tuple(l.strip() for l in sys.stdin)

initial = tuple(p == "#" for p in lines[0][15:])
rules = dict((tuple(p == "#" for p in l[:5]), l[9] == "#") for l in lines[2:])

pots = dict(zip(range(len(initial)), initial))

gens = [pots]

min_plant = 0
max_plant = len(initial)
last_sum = 0
diff = 0
last_diff = 0
sim = 1000
for g in range(sim):
    new_pots = {}
    for i in range(min_plant - 2, max_plant + 2):
        was = tuple(pots.get(p, False) for p in range(i - 2, i + 3))
        new_pot = rules.get(was, False)
        if new_pot:
            if i < min_plant:
                min_plant = i
            if i > max_plant:
                max_plant = i
            new_pots[i] = new_pot
    pots = new_pots
    sum = 0
    count = 0
    for p in pots.keys():
        sum += p
        count += 1
    diff = sum - last_sum
    print(g, sum, diff, count)
    gens.append(pots)
    if diff == last_diff:
        break
    last_diff = diff
    last_sum = sum

print(last_sum + (50000000000 - sim) * diff)

print(min_plant, max_plant)

scale = 4
image = Image.new("1", (scale * (max_plant - min_plant + 1), scale * len(gens)))
data = []
for gen in gens:
    width = 0
    row = []
    for p in range(min_plant, max_plant + 1):
        if gen.get(p, False):
            row += [1] * scale
        else:
            row += [0] * scale
        width += 1
    data += row * scale
    # print(width)
# image.putdata(tuple(i % 2 for i in range((max_plant - min_plant) * len(gens))))
image.putdata(data)
image.save("out.png")

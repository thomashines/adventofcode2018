#!/usr/bin/env python3

import sys

lines = tuple(l.strip() for l in sys.stdin)

initial = lines[0][15:]
rules = dict((tuple(p == "#" for p in l[:5]), l[9] == "#") for l in lines[2:])

print(initial)
print(rules)

pots = {}
for i in range(len(initial)):
    pots[i] = initial[i] == "#"

def string_pots(pots):
    stringed = ""
    for p in range(-10, 40):
        if pots.get(p, False):
            stringed += "#"
        else:
            stringed += "."
    return stringed

gens = [string_pots(pots)]

min_plant = 0
max_plant = len(initial)
for g in range(20):
    new_pots = {}
    for i in range(min_plant - 2, max_plant + 2):
        was = tuple(pots.get(p, False) for p in range(i - 2, i + 3))
        new_pot = rules.get(was, False)
        if new_pot:
            if i < min_plant:
                min_plant = i
            if i > max_plant:
                max_plant = i
        new_pots[i] = new_pot
    pots = new_pots
    gens.append(string_pots(pots))

for gen in gens:
    print(gen)

sum = 0
for p, yes in pots.items():
    if yes:
        sum += p

print(sum)

#!/usr/bin/env python3

import sys
import time

ops = {
    "addr": lambda m, a, b: m[a] + m[b],
    "addi": lambda m, a, b: m[a] + b,
    "mulr": lambda m, a, b: m[a] * m[b],
    "muli": lambda m, a, b: m[a] * b,
    "banr": lambda m, a, b: m[a] & m[b],
    "bani": lambda m, a, b: m[a] & b,
    "borr": lambda m, a, b: m[a] | m[b],
    "bori": lambda m, a, b: m[a] | b,
    "setr": lambda m, a, b: m[a],
    "seti": lambda m, a, b: a,
    "gtir": lambda m, a, b: 1 if a > m[b] else 0,
    "gtri": lambda m, a, b: 1 if m[a] > b else 0,
    "gtrr": lambda m, a, b: 1 if m[a] > m[b] else 0,
    "eqir": lambda m, a, b: 1 if a == m[b] else 0,
    "eqri": lambda m, a, b: 1 if m[a] == b else 0,
    "eqrr": lambda m, a, b: 1 if m[a] == m[b] else 0,
}

def do(m, op, a, b, c):
    m2 = list(m)
    m2[c] = ops[op](m2, a, b)
    return m2

inputs = tuple(l.strip() for l in sys.stdin)

ip = int(inputs[0][4:])
print("ip", ip)

program = tuple(tuple(input.split(" ")) for input in inputs[1:])
program = tuple(tuple((i[0],) + tuple(int(n) for n in i[1:])) for i in program)
print("program", program)

m = [1, 0, 0, 0, 0, 0]
while m[ip] >= 0 and m[ip] < len(program):
    print(m[ip], program[m[ip]], m)
    m = do(m, *program[m[ip]])
    m[ip] += 1
    # time.sleep(1)

print(m)

# [
#   0: A,
#   1: B,
#   2: C,
#   3: D,
#   4: IP,
#   5: E,
# ]

# A = 1
# B = 0
# C = 0
# D = 0
# IP = 0
# E = 0

# E = ((E + 2) ** 2) * 19 * 11
# C = (C + 4) * 22 + E
# E = E + C = 10551329, somehow

# C = 924
# D = 1
# B = 1 # 2:
# C = D * B # 3:
# if C == 10551329
#   A = D + A
# if B > 10551329
#   D = D + 1
#   if D > 10551329
#       exit
#   goto 2
# goto 3

# while True:
#     if C = 10551329:
#         A = D + A
#     B = B + 1
#     if B > 10551329:
#         D = D + 1
#         if D > 10551329:
#             exit
#         B = 1
#     C = D * B



#ip 4
#  0: addi 4 16 4 # IP = IP + 16 (IP = 0 + 16 + 1, go to 17)
#  1: seti 1 5 3  # D = 1
#  2: seti 1 9 1  # B = 1
#  3: mulr 3 1 2  # C = D * B
#  4: eqrr 2 5 2  # C = 1 if C == E else 0
#  5: addr 2 4 4  # IP = IP + C (IP = 5 + (0 or 1) + 1, go to 6 or 7)
#  6: addi 4 1 4  # IP = IP + 1 (IP = 6 + 1 + 1, go to 8)
#  7: addr 3 0 0  # A = D + A
#  8: addi 1 1 1  # B = B + 1
#  9: gtrr 1 5 2  # C = 1 if B > E else 0
# 10: addr 4 2 4  # IP = IP + C (IP = 10 + (0 or 1) + 1, go to 11 or 12)
# 11: seti 2 9 4  # IP = 2 (IP = 2 + 1, go to 3)
# 12: addi 3 1 3  # D = D + 1
# 13: gtrr 3 5 2  # C = 1 if D > E else 0
# 14: addr 2 4 4  # IP = C + IP (IP = 14 + (0 or 1) + 1, go to 15 or 16)
# 15: seti 1 8 4  # IP = 1 (IP = 1 + 1, go to 2)
# 16: mulr 4 4 4  # Exit
# 17: addi 5 2 5  # E = E + 2
# 18: mulr 5 5 5  # E = E * E
# 19: mulr 4 5 5  # E = IP * E (E = 19 * E)
# 20: muli 5 11 5 # E = E * 11
# 21: addi 2 4 2  # C = C + 4
# 22: mulr 2 4 2  # C = C * IP (C = C * 22)
# 23: addi 2 5 2  # C = C + E
# 24: addr 5 2 5  # E = E + C
# 25: addr 4 0 4  # IP = IP + 0 (IP = 25 + 0 + 1, go to 26)
# 26: seti 0 9 4  # IP = 0 (IP = 0 + 1, go to 1)
# 27: setr 4 2 2
# 28: mulr 2 4 2
# 29: addr 4 2 2
# 30: mulr 4 2 2
# 31: muli 2 14 2
# 32: mulr 2 4 2
# 33: addr 5 2 5
# 34: seti 0 0 0
# 35: seti 0 8 4

# 3 ('mulr', 3, 1, 2) [0, 6335297, 0, 1, 3, 10551329]
# 4 ('eqrr', 2, 5, 2) [0, 6335297, 6335297, 1, 4, 10551329]
# 5 ('addr', 2, 4, 4) [0, 6335297, 0, 1, 5, 10551329]
# 6 ('addi', 4, 1, 4) [0, 6335297, 0, 1, 6, 10551329]
# 8 ('addi', 1, 1, 1) [0, 6335297, 0, 1, 8, 10551329]
# 9 ('gtrr', 1, 5, 2) [0, 6335298, 0, 1, 9, 10551329]
# 10 ('addr', 4, 2, 4) [0, 6335298, 0, 1, 10, 10551329]
# 11 ('seti', 2, 9, 4) [0, 6335298, 0, 1, 11, 10551329]
# 3 ('mulr', 3, 1, 2) [0, 6335298, 0, 1, 3, 10551329]
# 4 ('eqrr', 2, 5, 2) [0, 6335298, 6335298, 1, 4, 10551329]
# 5 ('addr', 2, 4, 4) [0, 6335298, 0, 1, 5, 10551329]
# 6 ('addi', 4, 1, 4) [0, 6335298, 0, 1, 6, 10551329]
# 8 ('addi', 1, 1, 1) [0, 6335298, 0, 1, 8, 10551329]
# 9 ('gtrr', 1, 5, 2) [0, 6335299, 0, 1, 9, 10551329]
# 10 ('addr', 4, 2, 4) [0, 6335299, 0, 1, 10, 10551329]
# 11 ('seti', 2, 9, 4) [0, 6335299, 0, 1, 11, 10551329]
# 3 ('mulr', 3, 1, 2) [0, 6335299, 0, 1, 3, 10551329]
# 4 ('eqrr', 2, 5, 2) [0, 6335299, 6335299, 1, 4, 10551329]
# 5 ('addr', 2, 4, 4) [0, 6335299, 0, 1, 5, 10551329]
# 6 ('addi', 4, 1, 4) [0, 6335299, 0, 1, 6, 10551329]
# 8 ('addi', 1, 1, 1) [0, 6335299, 0, 1, 8, 10551329]
# 9 ('gtrr', 1, 5, 2) [0, 6335300, 0, 1, 9, 10551329]
# 10 ('addr', 4, 2, 4) [0, 6335300, 0, 1, 10, 10551329]
# 11 ('seti', 2, 9, 4) [0, 6335300, 0, 1, 11, 10551329]
# 3 ('mulr', 3, 1, 2) [0, 6335300, 0, 1, 3, 10551329]
# 4 ('eqrr', 2, 5, 2) [0, 6335300, 6335300, 1, 4, 10551329]
# 5 ('addr', 2, 4, 4) [0, 6335300, 0, 1, 5, 10551329]
# 6 ('addi', 4, 1, 4) [0, 6335300, 0, 1, 6, 10551329]
# 8 ('addi', 1, 1, 1) [0, 6335300, 0, 1, 8, 10551329]
# 9 ('gtrr', 1, 5, 2) [0, 6335301, 0, 1, 9, 10551329]
# 10 ('addr', 4, 2, 4) [0, 6335301, 0, 1, 10, 10551329]
# 11 ('seti', 2, 9, 4) [0, 6335301, 0, 1, 11, 10551329]
# 3 ('mulr', 3, 1, 2) [0, 6335301, 0, 1, 3, 10551329]
# 4 ('eqrr', 2, 5, 2) [0, 6335301, 6335301, 1, 4, 10551329]
# 5 ('addr', 2, 4, 4) [0, 6335301, 0, 1, 5, 10551329]
# 6 ('addi', 4, 1, 4) [0, 6335301, 0, 1, 6, 10551329]
# 8 ('addi', 1, 1, 1) [0, 6335301, 0, 1, 8, 10551329]
# 9 ('gtrr', 1, 5, 2) [0, 6335302, 0, 1, 9, 10551329]
# 10 ('addr', 4, 2, 4) [0, 6335302, 0, 1, 10, 10551329]
# 11 ('seti', 2, 9, 4) [0, 6335302, 0, 1, 11, 10551329]

#!/usr/bin/env python3

import sys

claims = {}
for line in sys.stdin:
    # #1287 @ 138,557: 18x28
    id, _m, pos, size = line.strip().split(" ")
    claims[id[1:]] = {
        "id": id[1:],
        "pos": tuple(int(p) for p in pos[:-1].split(",")),
        "size": tuple(int(s) for s in size.split("x")),
    }

size = 1000
squares = [[0 for i in range(size)] for j in range(size)]

for id, claim in claims.items():
    for x in range(claim["pos"][0], claim["pos"][0] + claim["size"][0]):
        for y in range(claim["pos"][1], claim["pos"][1] + claim["size"][1]):
            squares[x][y] += 1

for id, claim in claims.items():
    touching = False
    for x in range(claim["pos"][0], claim["pos"][0] + claim["size"][0]):
        for y in range(claim["pos"][1], claim["pos"][1] + claim["size"][1]):
            if squares[x][y] > 1:
                touching = True
    if not touching:
        print(id)

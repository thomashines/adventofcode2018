#!/usr/bin/env python

import sys

c = tuple(int(l) for l in sys.stdin)
i = 0
r = set((0,))
f = 0
while True:
    f += c[i]
    if f in r:
        print(f)
        break
    else:
        r.add(f)
    i = (i + 1) % len(c)

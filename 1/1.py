#!/usr/bin/env python

import sys

print(sum(int(l) for l in sys.stdin))

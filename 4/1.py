#!/usr/bin/env python3

import sys

guard_id = None
asleep = False
asleep_since = None

guards = {}

for line in sorted(sys.stdin):
    minute = int(line[15:17])
    if "Guard" in line:
        # [1518-03-28 00:04] Guard #2663 begins shift
        guard_id = int(line.split(" ")[3][1:])
        if guard_id not in guards:
            guards[guard_id] = {
                "sleeping_minutes": [],
            }
        asleep = False
    elif guard_id is not None and "falls asleep" in line:
        asleep = True
        asleep_since = minute
    elif guard_id is not None and "wakes up" in line and asleep:
        asleep = False
        guards[guard_id]["sleeping_minutes"] += list(range(asleep_since, minute))

most_sleepy = None
for guard_id, guard in guards.items():
    if most_sleepy is None or len(guard["sleeping_minutes"]) > len(guards[most_sleepy]["sleeping_minutes"]):
        most_sleepy = guard_id

print(most_sleepy)

minute_counts = {}
max_minute = None
for minute in guards[most_sleepy]["sleeping_minutes"]:
    if minute in minute_counts:
        minute_counts[minute] += 1
    else:
        minute_counts[minute] = 1
    if max_minute is None or minute_counts[minute] > minute_counts[max_minute]:
        max_minute = minute

print(max_minute)
print(most_sleepy * max_minute)

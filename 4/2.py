#!/usr/bin/env python3

import sys

guard_id = None
asleep = False
asleep_since = None

guards = {}

for line in sorted(sys.stdin):
    minute = int(line[15:17])
    if "Guard" in line:
        # [1518-03-28 00:04] Guard #2663 begins shift
        guard_id = int(line.split(" ")[3][1:])
        if guard_id not in guards:
            guards[guard_id] = {
                "sleeping_minutes": [],
            }
        asleep = False
    elif guard_id is not None and "falls asleep" in line:
        asleep = True
        asleep_since = minute
    elif guard_id is not None and "wakes up" in line and asleep:
        asleep = False
        guards[guard_id]["sleeping_minutes"] += list(range(asleep_since, minute))

candidates = []
for guard_id, guard in guards.items():
    minute_counts = {}
    max_minute = None
    for minute in guard["sleeping_minutes"]:
        if minute in minute_counts:
            minute_counts[minute] += 1
        else:
            minute_counts[minute] = 1
        if max_minute is None or minute_counts[minute] > minute_counts[max_minute]:
            max_minute = minute
    if max_minute is not None:
        candidates.append((guard_id, max_minute, minute_counts[max_minute]))

max_count = 0
max_guard_id = 0
max_max_minute = 0
for guard_id, max_minute, count in candidates:
    if count > max_count:
        max_count = count
        max_guard_id = guard_id
        max_max_minute = max_minute

print(max_guard_id * max_max_minute)

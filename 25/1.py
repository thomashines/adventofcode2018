#!/usr/bin/env python3

import sys

def l1(a, b):
    return sum(abs(bx - ax) for ax, bx in zip(a, b))

def constellate(a, b, groups, point_group):
    a_group = point_group.get(a)
    b_group = point_group.get(b)
    new_group = a_group or b_group or len(groups)
    if new_group >= len(groups):
        groups.append(set())
    if a_group is not None and a_group != new_group:
        while groups[a_group]:
            node = groups[a_group].pop()
            groups[new_group].add(node)
            point_group[node] = new_group
    if b_group is not None and b_group != new_group:
        while groups[b_group]:
            node = groups[b_group].pop()
            groups[new_group].add(node)
            point_group[node] = new_group
    groups[new_group].add(a)
    groups[new_group].add(b)
    point_group[a] = new_group
    point_group[b] = new_group

nodes = set()
groups = []
node_group = {}

for line in sys.stdin:
    node = tuple(int(d) for d in line.strip().split(","))
    nodes.add(node)
    groups.append(set((node,)))
    node_group[node] = len(groups) - 1

for node in nodes:
    for other in nodes:
        if node != other:
            if l1(node, other) <= 3:
                constellate(node, other, groups, node_group)

groups = [g for g in groups if len(g) > 0]

print(len(groups))

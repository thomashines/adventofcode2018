#!/usr/bin/env python3

import sys
from heapq import heappush, heappop

input = tuple(l.strip() for l in sys.stdin)
depth = int(input[0][7:])
t_x, t_y = (int(n) for n in input[1][8:].split(","))
print(depth, t_x, t_y)

cave = {}

def cave_get_erosion(cave, x, y, depth, tx, t_y):
    if (x, y) in cave:
        return cave[x, y]
    geo = None
    if (x, y) == (0, 0):
        geo = 0
    elif (x, y) == (t_x, t_y):
        geo = 0
    elif y == 0:
        geo = x * 16807
    elif x == 0:
        geo = y * 48271
    else:
        geo = cave_get_erosion(cave, x - 1, y, depth, t_x, t_y) * cave_get_erosion(cave, x, y - 1, depth, t_x, t_y)
    erosion = (geo + depth) % 20183
    cave[x, y] = erosion
    return erosion

def print_cave(cave, depth, t_x, t_y):
    for y in range(t_y):
        stringed = ""
        for x in range(t_x):
            stringed += {0: ".", 1: "=", 2: "|"}[cave_get_erosion(cave, x, y, depth, t_x, t_y) % 3]
        print(stringed)

def get_risk(cave, depth, t_x, t_y):
    risk = 0
    for y in range(t_y):
        for x in range(t_x):
            risk += cave_get_erosion(cave, x, y, depth, t_x, t_y) % 3
    return risk

# print_cave(cave, depth, t_x, t_y)
print(get_risk(cave, depth, t_x, t_y))

shortest = {}
pq = []
heappush(pq, (0, 0, 0, 0, 0, 0))
while len(pq) > 0:
    expected, time, x, y, gear, type = heappop(pq)
    if (x, y, gear) not in shortest:
        shortest[x, y, gear] = time
        if (x, y, gear) == (t_x, t_y, 0):
            break
        for dx, dy in ((0, 0), (0, -1), (0, +1), (-1, 0), (+1, 0)):
            a_x, a_y = x + dx, y + dy
            if a_x >= 0 and a_x <= 7 * t_x and a_y >= 0:
                new_type = cave_get_erosion(cave, a_x, a_y, depth, t_x, t_y) % 3
                for new_gear in (0, 1, 2):
                    if (a_x, a_y, new_gear) in shortest:
                        # Already visited
                        continue
                    if (type == 0 or new_type == 0) and new_gear == 2:
                        # Can't use neither in rocky
                        continue
                    if (type == 1 or new_type == 1) and new_gear == 0:
                        # Can't use torch in wet
                        continue
                    if (type == 2 or new_type == 2) and new_gear == 1:
                        # Can't use climbing gear in narrow
                        continue
                    dtime = 1
                    if gear != new_gear:
                        dtime += 7
                    heappush(pq, (time + dtime + abs(t_x - a_x) + abs(t_y - a_y), time + dtime, a_x, a_y, new_gear, new_type))

print(shortest.get((t_x, t_y, 0)))

# Search limits
print("search min x", min(shortest.keys(), key=lambda k: k[0])[0])
print("search max x", max(shortest.keys(), key=lambda k: k[0])[0])
print("search min y", min(shortest.keys(), key=lambda k: k[1])[1])
print("search max y", max(shortest.keys(), key=lambda k: k[1])[1])

#!/usr/bin/env python

import sys

barcodes = tuple(l.strip() for l in sys.stdin)

for a in barcodes:
    for b in barcodes:
        if a == b:
            continue
        diffs = 0
        for c in range(len(a)):
            if a[c] != b[c]:
                diffs += 1
            if diffs > 1:
                break
        if diffs == 1:
            print(a, b)

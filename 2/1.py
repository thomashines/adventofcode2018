#!/usr/bin/env python

import sys

barcodes = tuple(l.strip() for l in sys.stdin)

def lcount(s):
    counts = {}
    for c in s:
        if c in counts:
            counts[c] += 1
        else:
            counts[c] = 1
    return counts

lcounts = tuple((s, lcount(s)) for s in barcodes)

n2 = set()
n3 = set()

for barcode, lcount in lcounts:
    for l, c in lcount.items():
        if c == 2:
            n2.add(barcode)
        elif c == 3:
            n3.add(barcode)

print(len(n2))
print(len(n3))
print(len(n2) * len(n3))

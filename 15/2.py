#!/usr/bin/env python3

from heapq import heappop, heappush
import sys
import time

width = 0
height = 0

y = 0
initial = {}
for line in (l.strip() for l in sys.stdin):
    for x in range(len(line)):
        if line[x] == "#":
            initial[x, y] = {
                "type": "wall",
                "pos": (x, y),
            }
        elif line[x] in "EG":
            is_elf = line[x] == "E"
            initial[x, y] = {
                "type": {True: "elf", False: "goblin"}[is_elf],
                "pos": (x, y),
                "ap": 3,
                "hp": 200,
            }
            if is_elf:
                initial[x, y]["ap"] = 3
    width = len(line)
    y += 1

height = y

def print_lu(lu):
    for y in range(height):
        row_stringed = ""
        for x in range(width):
            thing = lu.get((x, y))
            if thing is None:
                row_stringed += " "
            else:
                row_stringed += {
                    "wall": "#",
                    "elf": "E",
                    "goblin": "G",
                }[thing["type"]]
        print(row_stringed)

print_lu(initial)

def get_nearest_mh(lu, start, type):
    x0, y0 = start
    ordered = {}
    done = False
    min_mh = None
    for other in lu.values():
        if other["type"] == type:
            x, y = other["pos"]
            for dx, dy in ((0, -1), (0, +1), (-1, 0), (+1, 0)):
                a_x, a_y = x + dx, y + dy
                adjacent = lu.get((a_x, a_y))
                if adjacent is None:
                    mh = abs(a_x - x0) + abs(a_y - y0)
                    if min_mh is None or mh <= min_mh:
                        min_mh = mh
                        ordered[mh, a_y, a_x] = True
                        if mh == 0:
                            done = True
                            break
        if done:
            break
    if len(ordered) > 0:
        return min(ordered)[0]

get_nearest_cache = {}

def get_nearest(lu, start, type):
    cached = get_nearest_cache.get((start, type))
    if cached is not None:
        return cached

    visited = {
        # start: True
    }
    pq = []
    nearest_mh = get_nearest_mh(lu, start, type)
    if nearest_mh is None:
        return None
    last_path_id = 0
    paths = {
        last_path_id: [start]
    }
    heappush(pq, (nearest_mh, 0, start[1], start[0], last_path_id))
    # heappush(pq, (0, 0, start[1], start[0], last_path_id))
    best = None
    revisited = 0
    while len(pq) > 0:
        a_dist, dist, y, x, path_id = heappop(pq)
        if visited.get((x, y)):
            revisited += 1
            continue
        visited[x, y] = True
        at_dest = False
        for dx, dy in ((0, -1), (0, +1), (-1, 0), (+1, 0)):
            a_x, a_y = x + dx, y + dy
            if visited.get((a_x, a_y)) or (a_x < 0 or a_y < 0 or a_x >= width
                    or a_y >= height):
                continue
            adjacent = lu.get((a_x, a_y))
            if adjacent is None:
                nearest_mh = get_nearest_mh(lu, start, type)
                last_path_id += 1
                paths[last_path_id] = paths[path_id] + [(a_x, a_y)]
                heappush(pq, (dist + 1 + nearest_mh, dist + 1, a_y, a_x, last_path_id))
                # heappush(pq, (dist + 1, dist + 1, a_y, a_x, last_path_id))
            elif adjacent["type"] == type:
                at_dest = True
                break
        if at_dest:
            best = (dist, (x, y), paths[path_id])
            break
    # print("revisited", revisited)
    # print("last_path_id", last_path_id)
    get_nearest_cache[start, type] = best
    return best

done = False
elf_ap = 3
while not done:
    elf_death = False
    lu = dict(initial)
    get_nearest_cache = {}
    elves = []
    goblins = []
    for pos, thing in lu.items():
        thing["pos"] = pos
        if thing["type"] == "elf":
            thing["hp"] = 200
            thing["ap"] = elf_ap
            elves.append(thing)
        elif thing["type"] == "goblin":
            thing["hp"] = 200
            goblins.append(thing)

    print_lu(lu)

    for round in range(1, 100000):
        ordered = {}
        for elf in elves:
            if elf["hp"] > 0:
                ordered[elf["pos"][1], elf["pos"][0]] = elf
        for goblin in goblins:
            if goblin["hp"] > 0:
                ordered[goblin["pos"][1], goblin["pos"][0]] = goblin
        for y, x in sorted(ordered.keys()):
            thing = ordered[y, x]
            if thing["hp"] <= 0:
                continue
            # print("turn", x, y, thing["type"], thing["hp"])

            enemy = {
                "elf": "goblin",
                "goblin": "elf",
            }[thing["type"]]

            should_move = True
            for dx, dy in ((0, -1), (0, +1), (-1, 0), (+1, 0)):
                a_x, a_y = x + dx, y + dy
                adjacent = lu.get((a_x, a_y))
                if adjacent is not None and adjacent["type"] == enemy:
                    should_move = False
                    break

            if should_move:
                # Move to nearest enemy
                # print("move to", enemy)
                nearest = get_nearest(lu, (x, y), enemy)
                if nearest is not None:
                    dist, dest, path = nearest
                    if dest != (x, y):
                        # print(dist, dest, path)
                        move_to = path[1]
                        del lu[x, y]
                        get_nearest_cache = {}
                        lu[move_to] = thing
                        x, y = move_to
                        thing["pos"] = move_to

            # Check for adjacent targets
            targets = {}
            for dx, dy in ((0, -1), (0, +1), (-1, 0), (+1, 0)):
                a_x, a_y = x + dx, y + dy
                adjacent = lu.get((a_x, a_y))
                if adjacent is not None and adjacent["type"] == enemy:
                    targets[adjacent["hp"], a_y, a_x] = adjacent
            if len(targets) > 0:
                t_hp, t_y, t_x = min(targets.keys())
                target = targets.get((t_hp, t_y, t_x))
                # print("target", t_x, t_y, target["type"])
                target["hp"] -= thing["ap"]
                if target["hp"] <= 0:
                    if target["type"] == "elf":
                        print("dead", target)
                        elf_death = True
                        break
                    del lu[target["pos"]]
                    get_nearest_cache = {}

        if elf_death:
            break

        elf_count = 0
        elf_hp = 0
        for elf in elves:
            if elf["hp"] > 0:
                elf_count += 1
                elf_hp += elf["hp"]
        goblin_count = 0
        goblin_hp = 0
        for goblin in goblins:
            if goblin["hp"] > 0:
                goblin_count += 1
                goblin_hp += goblin["hp"]
        print("after round", round)
        print_lu(lu)
        # print("goblins: ", goblin_count, goblin_hp)
        # print("elves: ", elf_count, elf_hp)
        if goblin_count == 0 or elf_count == 0:
            print("after round", round)
            print("elf_ap", elf_ap)
            print("goblins: ", goblin_count, goblin_hp)
            print("elves: ", elf_count, elf_hp)
            print("goblins: ", round * goblin_hp)
            print("elves: ", round * elf_hp)
            for y, x in sorted(ordered.keys()):
                thing = ordered[y, x]
                if thing["hp"] <= 0:
                    continue
                print(x, y, thing["type"], thing["hp"])
            done = True
            break
        # time.sleep(0.1)

    elf_ap += 1

    if done:
        break

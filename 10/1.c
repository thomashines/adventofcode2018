#include "inttypes.h"
#include "limits.h"
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

typedef struct Point {
  int64_t x, y, dx, dy;
} Point;

void step(Point *point, int64_t steps) {
  point->x += steps * point->dx;
  point->y += steps * point->dy;
}

void step_all(Point *points, int count, int64_t steps) {
  for (int p = 0; p < count; p++) {
    step(&points[p], steps);
  }
}

void limits(Point *points, int count, int64_t *min_x, int64_t *max_x,
    int64_t *min_y, int64_t *max_y) {
  *min_x = points[0].x;
  *max_x = points[0].x;
  *min_y = points[0].y;
  *max_y = points[0].y;
  for (int p = 1; p < count; p++) {
    if (points[p].x < *min_x) {
      *min_x = points[p].x;
    }
    if (points[p].x > *max_x) {
      *max_x = points[p].x;
    }
    if (points[p].y < *min_y) {
      *min_y = points[p].y;
    }
    if (points[p].y > *max_y) {
      *max_y = points[p].y;
    }
  }
}

void print_all(Point *points, int count) {
  int64_t min_x, max_x, min_y, max_y;
  limits(points, count, &min_x, &max_x, &min_y, &max_y);
  uint64_t width = max_x - min_x + 1;
  uint64_t height = max_y - min_y + 1;
  char **lines = malloc(height * sizeof(char *));
  for (int r = 0; r < height; r++) {
    lines[r] = malloc((width + 1) * sizeof(char));
    memset(lines[r], '.', width);
    lines[r][width] = 0;
  }
  for (int p = 0; p < count; p++) {
    lines[points[p].y - min_y][points[p].x - min_x] = '#';
  }
  for (int r = 0; r < height; r++) {
    printf("%s\n", lines[r]);
    free(lines[r]);
  }
  free(lines);
}

uint64_t bounding_area(Point *points, int count) {
  int64_t min_x, max_x, min_y, max_y;
  limits(points, count, &min_x, &max_x, &min_y, &max_y);
  uint64_t width = max_x - min_x;
  uint64_t height = max_y - min_y;
  return height * width;
}

void solve(Point *points, int count) {
  // Get initial area
  uint64_t area = bounding_area(points, count);
  printf("area = %u\n", area);
  // Repeat
  int steps = 1;
  int total_steps = 0;
  int last_decrease = 0;
  for (int i = 0; i < 100; i++) {
    if (i > last_decrease + 4) {
      break;
    }
    // Step forward
    step_all(points, count, steps);
    total_steps += steps;
    // Get new area
    uint64_t new_area = bounding_area(points, count);
    printf("%d:\n\tsteps = %d\n\t    area = %lu\n\tnew_area = %lu\n", i, steps,
      area, new_area);
    // If area decreased, double step size
    if (new_area < area) {
      last_decrease = i;
      steps *= 2;
    }
    // If area increased, step back, reverse and go to 1 steps
    if (new_area > area) {
      step_all(points, count, -steps);
      total_steps -= steps;
      if (steps > 0) {
        steps = -1;
      } else {
        steps = 1;
      }
    }
    area = bounding_area(points, count);;
  }

  printf("total_steps=%d\n", total_steps);

  // Print the state
  print_all(points, count);

  return;
}

int main(int argc, char **argv) {
  int size = 1;
  Point *points = malloc(size * sizeof(Point));
  int point = 0;

  size_t length = 0;
  char *line = NULL;
  while (getline(&line, &length, stdin) != -1) {
    if (point >= size) {
      size *= 2;
      points = realloc(points, size * sizeof(Point));
    }

    char *end;
    points[point].x = strtol(line + 10, &end, 10);
    points[point].y = strtol(end + 1, &end, 10);
    points[point].dx = strtol(end + 12, &end, 10);
    points[point].dy = strtol(end + 1, &end, 10);
    printf("%d: <%d, %d> <%d, %d>\n", point, points[point].x, points[point].y,
      points[point].dx, points[point].dy);
    point++;

    free(line);
    length = 0;
    line = NULL;
  }

  solve(points, point);

  free(points);
  return 0;
}

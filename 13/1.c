#include "inttypes.h"
#include "limits.h"
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

typedef enum Type {
  CORNER_F = '/',
  CORNER_B = '\\',
  INTERSECTION = '+',
  VERTICAL = '|',
  HORIZONTAL = '/',
  CART = 'c',
  NOTHING = ' ',
} Type;

typedef enum Turn {
  LEFT = 0,
  STRAIGHT = 1,
  RIGHT = 2
} Turn;

typedef struct Thing {
  int x, y;
  int dx, dy;
  Turn turn;
  char sym;
  char dead;
  char moved;
  Type type;
} Thing;

typedef struct Things {
  int count, size;
  Thing *things;
} Things;

Things *things_new(void) {
  Things *things = malloc(sizeof(Things));
  things->count = 0;
  things->size = 1;
  things->things = malloc(things->size * sizeof(Thing));
}

void things_free(Things *things) {
  free(things->things);
  free(things);
}

Thing *things_add(Things *things) {
  things->count++;
  if (things->count >= things->size) {
    things->size *= 2;
    things->things = realloc(things->things, things->size * sizeof(Thing));
  }
  return &things->things[things->count - 1];
}

char cart_sym(Thing *cart) {
  switch (cart->dx + 2 * cart->dy) {
    case -1:
      return '<';
    case +1:
      return '>';
    case -2:
      return '^';
    case +2:
      return 'v';
  }
}

void things_map_print(Thing ***map, Things *carts, int width, int height) {
  char *stringed = malloc(height * (width + 1) * sizeof(char) + 1);
  int c = 0;
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      if (map[y][x] == NULL) {
        stringed[c++] = ' ';
      } else {
        stringed[c++] = map[y][x]->sym;
      }
    }
    stringed[c++] = '\n';
  }
  stringed[c] = 0;
  for (int c = 0; c < carts->count; c++) {
    Thing *cart = &carts->things[c];
    stringed[cart->y * (width + 1) + cart->x] = cart_sym(cart);
  }
  printf("%s", stringed);
}

void solve(Things *things, int width, int height) {
  // Make matrix of track parts
  Thing **tracks = malloc(width * height * sizeof(Thing *));
  memset(tracks, 0, width * height * sizeof(Thing *));
  Thing ***map = malloc(height * sizeof(Thing **));
  for (int y = 0; y < height; y++) {
    map[y] = &tracks[width * y];
  }
  for (int t = 0; t < things->count; t++) {
    Thing *thing = &things->things[t];
    map[thing->y][thing->x] = thing;
  }

  // Make list of carts and fill in missing track parts
  Things *carts = things_new();
  for (int t = 0; t < things->count; t++) {
    Thing *thing = &things->things[t];
    if (thing->sym == '<' || thing->sym == '>' ||
        thing->sym == '^' || thing->sym == 'v') {
      Thing *cart = things_add(carts);
      cart->sym = 'c';
      cart->x = thing->x;
      cart->y = thing->y;
      cart->turn = LEFT;
      cart->dead = 0;
      cart->moved = 0;
      cart->type = CART;
      switch (thing->sym) {
        case '<': cart->dx = -1; cart->dy =  0; break;
        case '>': cart->dx = +1; cart->dy =  0; break;
        case '^': cart->dx =  0; cart->dy = -1; break;
        case 'v': cart->dx =  0; cart->dy = +1; break;
      }
      switch (thing->sym) {
        case '<': case '>': thing->sym = '-'; break;
        case '^': case 'v': thing->sym = '|'; break;
      }
    } else {
      thing->type = thing->sym;
    }
  }

  // Initial state
  // things_map_print(map, carts, width, height);

  char done = 0;
  while (!done) {
    // Find the topmost cart
    Thing *top_cart = NULL;
    int count = 0;
    for (int c = 0; c < carts->count; c++) {
      Thing *cart = &carts->things[c];
      if (!cart->dead) {
        count++;
        if (!cart->moved) {
          if (top_cart == NULL || cart->y < top_cart->y ||
              (cart->y == top_cart->y && cart->x < top_cart->x)) {
            top_cart = cart;
          }
        }
      }
    }
    // printf("count = %d\n", count);
    done = count == 0;
    if (done) {
      break;
    }
    if (top_cart == NULL) {
      for (int c = 0; c < carts->count; c++) {
        carts->things[c].moved = 0;
      }
      continue;
    }
    // Move the cart
    top_cart->x += top_cart->dx;
    top_cart->y += top_cart->dy;
    // Check for intersections and corners
    Thing *track = map[top_cart->y][top_cart->x];
    Turn turn = STRAIGHT;
    if (track != NULL) {
      switch (track->type) {
        case CORNER_F:
          if (top_cart->dy == 0) {
            turn = LEFT;
          } else {
            turn = RIGHT;
          }
          break;
        case CORNER_B:
          if (top_cart->dy == 0) {
            turn = RIGHT;
          } else {
            turn = LEFT;
          }
          break;
        case INTERSECTION:
          turn = top_cart->turn;
          switch (turn) {
            case LEFT:
              top_cart->turn = STRAIGHT;
              break;
            case STRAIGHT:
              top_cart->turn = RIGHT;
              break;
            case RIGHT:
              top_cart->turn = LEFT;
              break;
          }
          break;
      }
    }
    int swap;
    switch (turn) {
      case LEFT:
        swap = top_cart->dy;
        top_cart->dy = -top_cart->dx;
        top_cart->dx = swap;
        break;
      case RIGHT:
        swap = top_cart->dy;
        top_cart->dy = top_cart->dx;
        top_cart->dx = -swap;
        break;
    }
    top_cart->moved = 1;

    done = count == 1;
    if (done) {
      for (int c = 0; c < carts->count; c++) {
        Thing *cart = &carts->things[c];
        if (!cart->dead) {
          printf("x = %d, y = %d, dx = %d, dy = %d\n", cart->x, cart->y,
            cart->dx, cart->dy);
        }
      }
      break;
    }

    // things_map_print(map, carts, width, height);

    // Check for collisions
    for (int a = 0; a < carts->count - 1; a++) {
      Thing *cart_a = &carts->things[a];
      if (cart_a->dead) {
        continue;
      }
      for (int b = a + 1; b < carts->count; b++) {
        Thing *cart_b = &carts->things[b];
        if (cart_b->dead) {
          continue;
        }
        if (cart_a->x == cart_b->x && cart_a->y == cart_b->y) {
          cart_a->dead = 1;
          cart_b->dead = 1;
          printf("collision %d, %d\n", cart_a->x, cart_a->y);
        }
      }
    }
  }

  things_free(carts);
  free(tracks);
  free(map);
}

int main(int argc, char **argv) {
  Things *things = things_new();

  int height = 0, width = 0;

  int x = 0, y = 0;
  char done = 0;
  while (!done) {
    char sym = getchar();
    done = sym == EOF;
    if (!done) {
      if (sym == '\r' || sym == '\n') {
        // New line
        if (x > 0) {
          if (x >= width) {
            width = x + 1;
          }
          y++;
          x = 0;
        }
      } else if (sym == ' ') {
        // Nothing
        x++;
      } else {
        // Thing
        Thing *thing = things_add(things);
        thing->x = x;
        thing->y = y;
        thing->sym = sym;
        x++;
      }
    }
  }
  height = y;

  solve(things, width, height);

  things_free(things);

  return 0;
}

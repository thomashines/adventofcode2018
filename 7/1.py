#!/usr/bin/env python3

import sys

deps = tuple((line[36], line[5]) for line in sys.stdin)
print(deps)

steps = {}
for dep in deps:
    if dep[0] not in steps:
        steps[dep[0]] = {
            "before": []
        }
    if dep[1] not in steps:
        steps[dep[1]] = {
            "before": []
        }
    steps[dep[0]]["before"].append(dep[1])

order = 0
for step in sorted(steps.keys()):
    steps[step]["order"] = order
    order += 1

while True:
    in_order = True
    for step in reversed(sorted(steps.keys())):
        details = steps[step]
        for before in reversed(sorted(details["before"])):
            if details["order"] > steps[before]["order"]:
                in_order = False
                details["order"], steps[before]["order"] = steps[before]["order"], details["order"]
                break
        if not in_order:
            break
    if in_order:
        break

actual_order = {}
for step, details in steps.items():
    actual_order[details["order"]] = step

stringed = ""
for order in sorted(actual_order.keys()):
    print(actual_order[order])
    stringed += actual_order[order]
print(stringed)

done = set()
order = []
for _ in steps.keys():
    for step in sorted(steps.keys()):
        if step in done:
            continue
        available = True
        for before in steps[step]["before"]:
            if before not in done:
                available = False
                break
        if available:
            done.add(step)
            order.append(step)
            break
print("".join(order))

#!/usr/bin/env python3

import sys

deps = tuple((line[36], line[5]) for line in sys.stdin)
print(deps)

steps = {}
for dep in deps:
    if dep[0] not in steps:
        steps[dep[0]] = {
            "before": []
        }
    if dep[1] not in steps:
        steps[dep[1]] = {
            "before": []
        }
    steps[dep[0]]["before"].append(dep[1])

done = set()
order = []
for _ in steps.keys():
    for step in sorted(steps.keys()):
        if step in done:
            continue
        available = True
        for before in steps[step]["before"]:
            if before not in done:
                available = False
                break
        if available:
            done.add(step)
            order.append(step)
            break
print("".join(order))

workers = [[None, 0] for i in range(5)]
# workers = [[None, 0] for i in range(2)]

time = 0
doing = set()
done = set()
while len(done) != len(order):
    for worker in workers:
        if worker[0] is not None and time >= worker[1]:
            # print("{} done at {}".format(worker[0], time))
            done.add(worker[0])
            worker[0] = None
    # print(workers)
    next = None
    for step in order:
        if step in doing or step in done:
            continue
        available = True
        for before in steps[step]["before"]:
            if before not in done:
                available = False
                break
        if available:
            for worker in workers:
                if worker[0] is None:
                    worker[0] = step
                    worker[1] = time + 1 + ord(step) - ord("A")
                    worker[1] += 60
                    doing.add(step)
                    # print(worker)
                    break
    print(time, workers)
    time += 1
print(time - 1)

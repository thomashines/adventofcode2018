#!/usr/bin/env python3

import sys

inputs = tuple(l.strip() for l in sys.stdin)

for input in inputs:
    print(input)
    if len(input) == 0:
        continue
    input = int(input)
    recipes = [3, 7]
    elves = [0, 1]
    done = False
    while not done:
        new = sum(recipes[e] for e in elves)
        recipes += [int(d) for d in str(new)]
        if len(recipes) > input + 10:
            print("".join(str(d) for d in recipes[input:input+10]))
            done = True
        elves = [(e + recipes[e] + 1) % len(recipes) for e in elves]

#!/usr/bin/env python3

import sys

inputs = tuple(l.strip() for l in sys.stdin)

for input in inputs:
    print(input)
    if len(input) == 0:
        continue
    lookup = [int(d) for d in input]
    lookup_len = len(lookup)
    print(lookup)
    recipes = [3, 7]
    elves = [0, 1]
    done = False
    while not done:
        new = sum(recipes[e] for e in elves)
        new_recipes = [int(d) for d in str(new)]
        for recipe in new_recipes:
            recipes.append(recipe)
            if len(recipes) >= lookup_len and recipes[-lookup_len:] == lookup:
                print(len(recipes) - lookup_len)
                done = True
        elves = [(e + recipes[e] + 1) % len(recipes) for e in elves]

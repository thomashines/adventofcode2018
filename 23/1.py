#!/usr/bin/env python3

import sys
from heapq import heappush, heappop

input = tuple(l.strip() for l in sys.stdin)
nanobots = set()
for i in range(len(input)):
    pos, radius = input[i].split(", ")
    nanobot = (
        i,
        tuple(int(p) for p in pos[5:-1].split(",")),
        int(radius[2:]),
    )
    # print(nanobot)
    nanobots.add(nanobot)
print(len(nanobots))
strongest = max(nanobots, key=lambda nanobot: nanobot[2])
print(strongest)

def l1(a, b):
    return sum(abs(bx - ax) for (ax, bx) in zip(a, b))

in_range = set()
for id, pos, radius in nanobots:
    # print(id, pos, radius, l1(strongest[1], pos))
    if l1(strongest[1], pos) <= strongest[2]:
        in_range.add((id, pos, radius))

print(len(in_range))

# Just extract all the corners and centres and test them

def count_in_range(pos, nanobots):
    count = 0
    for id, other_pos, radius in nanobots:
        if l1(other_pos, pos) <= radius:
            count += 1
    return count

test_points = {}
for id, pos, radius in nanobots:
    x, y, z = pos
    for dx, dy, dz in ((0, 0, 0), (-radius, 0, 0), (+radius, 0, 0), (0, -radius, 0), (0, +radius, 0), (0, 0, -radius), (0, 0, +radius)):
        t_x, t_y, t_z = x + dx, y + dy, z + dz
        if (t_x, t_y, t_z) not in test_points:
            test_points[t_x, t_y, t_z] = count_in_range((t_x, t_y, t_z), nanobots)
print(len(test_points))

# Get the best test points
optimal = set()
optimal_count = 0
for test_point, count in test_points.items():
    if count == optimal_count:
        optimal.add(test_point)
    elif count > optimal_count:
        optimal_count = count
        optimal = set((test_point,))

print(optimal)
print(optimal_count)

# For each point in an optimal region, move toward the origin until it stops
# being optimal
optimaller = set()
for test_point in optimal:
    x, y, z = test_point
    # Move diagonally
    # step_dx = 0 if x == 0 else (1 if x < 0 else -1)
    # step_dy = 0 if y == 0 else (1 if y < 0 else -1)
    # step_dz = 0 if z == 0 else (1 if z < 0 else -1)
    steps = set()
    for dx in (-1, 0, +1):
        for dy in (-1, 0, +1):
            for dz in (-1, 0, +1):
                if dx == 0 and dy == 0 or dx == 0 and dz == 0 or dy == 0 and dz == 0 or dx != 0 and dy != 0 and dz != 0:
                    continue
                for d in range(1, 512):
                    steps.add((d * dx, d * dy, d * dz))
    steps = tuple(sorted(steps, key=lambda step: -l1((0, 0, 0), step)))
    # print(steps)
    best = test_point
    best_count = count_in_range(test_point, nanobots)
    best_dist = l1((0, 0, 0), test_point)
    closer = True
    while closer:
        closer = False
        x, y, z = best
        for dx, dy, dz in steps:
            a_x, a_y, a_z = x + dx, y + dy, z + dz
            count = count_in_range((a_x, a_y, a_z), nanobots)
            dist = l1((0, 0, 0), (a_x, a_y, a_z))
            if count > best_count or (count == best_count and dist < best_dist):
                best = (a_x, a_y, a_z)
                best_count = count
                best_dist = dist
                closer = True
                print(best, best_count, best_dist)
                break
    optimaller.add((best, best_count, best_dist))

(x, y, z), best_count, best_dist = min(optimaller, key=lambda o: o[2])
print((x, y, z), best_count, best_dist)

for p_y in range(y - 10, y + 10):
    line = []
    for p_x in range(x - 10, x + 10):
        line.append(count_in_range((p_x, p_y, z), nanobots) - optimal_count)
    print(" ".join("{: 3}".format(c) for c in line))

#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

typedef struct Marble {
  struct Marble *cw;
  uint64_t value;
  struct Marble *acw;
} Marble;

Marble *rotate(Marble *from, int64_t count) {
  while (count > 0) {
    from = from->cw;
    count--;
  }
  while (count < 0) {
    from = from->acw;
    count++;
  }
  return from;
}

void print_all(Marble *marble, Marble *start) {
  if (start == NULL) {
    start = marble;
  } else if (start == marble) {
    printf("\n");
    return;
  }
  printf("%u ", marble->value);
  print_all(marble->acw, start);
}

uint64_t solve(uint64_t players, uint64_t last_marble) {
  uint64_t *scores = malloc(players * sizeof(uint64_t));
  memset(scores, 0, players * sizeof(uint64_t));
  uint64_t player = 0;

  Marble *marbles = malloc((last_marble + 1) * sizeof(Marble));
  Marble *current = &marbles[0];
  current->cw = current;
  current->value = 0;
  current->acw = current;
  for (uint64_t m = 1; m <= last_marble; m++) {
    Marble *new = &marbles[m];
    new->value = m;

    uint64_t old_score = scores[player];

    if (m % 23 == 0) {
      scores[player] += new->value;
      Marble *remove = rotate(current, -7);
      scores[player] += remove->value;
      current = remove->cw;
      current->acw = remove->acw;
      current->acw->cw = current;
    } else {
      Marble *insert_after = rotate(current, 1);
      Marble *insert_before = insert_after->cw;

      insert_after->cw = new;
      new->acw = insert_after;
      new->cw = insert_before;
      insert_before->acw = new;

      current = new;
    }

    player = (player + 1) % players;
  }

  uint64_t high_score = 0;
  for (uint64_t player = 0; player < players; player++) {
    if (scores[player] > high_score) {
      high_score = scores[player];
    }
  }

  return high_score;
}

int main(int argc, char **argv) {
  size_t length = 0;
  char *line = NULL;
  while (getline(&line, &length, stdin) != -1) {
    char *end;
    uint64_t players = strtol(line, &end, 10);
    uint64_t marbles = strtol(end + 31, &end, 10);
    uint64_t high_score = strtol(end + 23, &end, 10);
    printf("solve(%u, %u) = %u\n", players, marbles, high_score);
    printf("%u\n", solve(players, marbles));
    free(line);
    length = 0;
    line = NULL;
  }
  return 0;
}

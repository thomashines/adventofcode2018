#!/usr/bin/env python3

import sys
import time
from heapq import heappop, heappush

inputs = tuple(l.strip() for l in sys.stdin)

def print_grid(grid):
    min_x = min(grid.keys(), key=lambda p: p[0])[0]
    max_x = max(grid.keys(), key=lambda p: p[0])[0]
    min_y = min(grid.keys(), key=lambda p: p[1])[1]
    max_y = max(grid.keys(), key=lambda p: p[1])[1]
    stringed = [["#" for x in range(2 * (max_x - min_x + 1) + 1)]
        for y in range(2 * (max_y - min_y + 1) + 1)]

    for (x, y), node in grid.items():
        x, y = x - min_x, y - min_y
        stringed[2 * y + 1][2 * x + 1] = str(node["max_doors"] % 10)
        for edge in node["edges"]:
            e_x, e_y = edge
            e_x, e_y = e_x - min_x, e_y - min_y
            d_x, d_y = (2 * x + 1 + 2 * e_x + 1) // 2, (2 * y + 1 + 2 * e_y + 1) // 2
            stringed[d_y][d_x] = "|"

    for line in stringed:
        print("".join(line))

def get_max_doors(grid, point=(0, 0), visited=set()):
    visited_this = visited.union(set((point,)))
    node = grid[point]
    # print(point, node, visited_this)
    max_max_doors = 0
    for edge in node["edges"]:
        if edge not in visited:
            max_doors = get_max_doors(grid, edge, visited_this) + 1
            if max_doors > max_max_doors:
                max_max_doors = max_doors
    return max_max_doors

def get_max_doors_flat(grid, start=(0, 0)):
    for node in grid.values():
        node["max_doors"] = 0

    max_max_doors = 0

    visited = set((0, 0))
    path = [(0, 0)]
    doors = {}
    while len(path) > 0:
        pos = path[-1]
        node = grid[pos]
        next_edge = None
        for edge in node["edges"]:
            if edge not in node["opened"]:
                next_edge = edge
                break
        if next_edge is None:
            # dead end, go back
            path.pop()
        else:
            next_node = grid[next_edge]
            node["opened"].add(next_edge)
            next_node["opened"].add(pos)
            if node["max_doors"] + 1 > next_node["max_doors"]:
                next_node["max_doors"] = node["max_doors"] + 1
                if next_node["max_doors"] > max_max_doors:
                    max_max_doors = next_node["max_doors"]
            path.append(next_edge)

    return max_max_doors

def get_min_doors_flat(grid, start=(0, 0)):
    pq = []
    heappush(pq, (0, start))
    visited = set()
    while len(pq) > 0:
        dist, pos = heappop(pq)
        if pos not in visited:
            visited.add(pos)
            node = grid[pos]
            node["min_doors"] = dist
            for edge in node["edges"]:
                if edge not in visited:
                    heappush(pq, (dist + 1, edge))

for input in inputs:
    print(input)
    max_max_doors = 0
    # map = buildmap(input[1:-1])
    grid = {}
    start = (0, 0)
    grid[start] = {"edges": set(), "max_doors": 0, "opened": set()}
    stack = [start, start]
    for c in input[1:-1]:
        pos = stack[-1]
        if c == "(":
            # Push into stack
            stack.append(pos)
            pos = stack[-1]
        elif c == "|":
            # Return to previous position but keep stack at same depth
            pos = stack[-2]
        elif c == ")":
            # Pop from stack
            stack.pop()
            pos = stack[-1]
        else:
            x, y = pos
            dx, dy = {
                "E": (+1, 0),
                "N": (0, -1),
                "S": (0, +1),
                "W": (-1, 0),
            }[c]
            next_pos = x + dx, y + dy
            grid[pos]["edges"].add(next_pos)
            if next_pos not in grid:
                grid[next_pos] = {"edges": set(), "max_doors": 0, "opened": set()}
            grid[next_pos]["edges"].add(pos)
            if grid[pos]["max_doors"] >= grid[next_pos]["max_doors"]:
                grid[next_pos]["max_doors"] = grid[pos]["max_doors"] + 1
                if grid[next_pos]["max_doors"] > max_max_doors:
                    max_max_doors = grid[next_pos]["max_doors"]
            pos = next_pos
        stack[-1] = pos
    # print(grid)
    print(max_max_doors)
    # print(get_max_doors(grid))
    print(get_max_doors_flat(grid))
    # print_grid(grid)
    get_min_doors_flat(grid)
    gte1000 = 0
    for node in grid.values():
        if node["min_doors"] >= 1000:
            gte1000 += 1
    print(gte1000)
    # break

#!/usr/bin/env python3

from heapq import heappush, heappop
import sys

state = {}

for line in sys.stdin:
    raw_pos = dict(p.split("=") for p in line.strip().split(", "))
    pos = {}
    for x, xs in raw_pos.items():
        if ".." in xs:
            x0, x1 = sorted(int(n) for n in xs.split(".."))
            pos[x] = range(x0, x1 + 1)
        else:
            pos[x] = (int(xs),)
    for x in pos["x"]:
        for y in pos["y"]:
            state[x, y] = "#"

min_x = min(state.keys(), key=lambda k: k[0])[0]
max_x = max(state.keys(), key=lambda k: k[0])[0]
min_y = min(state.keys(), key=lambda k: k[1])[1]
max_y = max(state.keys(), key=lambda k: k[1])[1]

def count_water(state):
    count = 0
    count_solid = 0
    for (x, y), v in state.items():
        if y >= min_y and y <= max_y and v in "|~":
            count += 1
            if v == "~":
                count_solid += 1
    return count, count_solid

def print_state(state):
    waters = tuple(k for k, v in state.items() if v in "|~")
    if len(waters) == 0:
        print("no water")
        return
    water_min_x = min(waters, key=lambda k: k[0])[0] - 1
    water_max_x = max(waters, key=lambda k: k[0])[0] + 1
    water_min_y = min(waters, key=lambda k: k[1])[1] - 1
    water_max_y = max(waters, key=lambda k: k[1])[1] + 1
    for y in range(water_min_y, water_max_y + 1):
        line = "{: 5d} ".format(y)
        for x in range(water_min_x, water_max_x + 1):
            line += state.get((x, y), " ")
        print(line)

print_state(state)

def find_corner(state, start, dir):
    x, y = start
    t, b = state.get((x, y)), state.get((x, y + 1))
    while x >= min_x and x <= max_x:
        if (t is not None and t != "|") or b is None or b not in "#~":
            break
        x += dir
        t, b = state.get((x, y)), state.get((x, y + 1))
    return (x, y), t, b

sources = []
state[500, 0] = "|"
heappush(sources, (-0, 500))

while len(sources) > 0:
    source = heappop(sources)
    y, x = source
    y = -y
    changed = False

    # Fall down to floor
    while y <= max_y:
        y += 1
        if (x, y) in state:
            break
        heappush(sources, (-y, x))
        state[x, y] = "|"
        changed = True

    # If fell off page, just go to next one
    if y > max_y:
        # if changed:
        #     # print_state(state)
        #     print(count_water(state), "waters")
        continue

    # Get corners to left and right
    y -= 1
    left, left_t, left_b = find_corner(state, (x, y), -1)
    right, right_t, right_b = find_corner(state, (x, y), +1)

    # If corners are solid, fill with solid water, else flowing water
    corner_parts = (left_t, left_b, right_t, right_b)
    solid = True
    for corner_part in corner_parts:
        if corner_part is None or corner_part == "|":
            solid = False
            break

    # Fill in level
    for x in range(left[0], right[0] + 1):
        if state.get((x, y)) != "#":
            if solid and state.get((x, y)) != "~":
                state[x, y] = "~"
                changed = True
            elif state.get((x, y)) is None:
                state[x, y] = "|"
                heappush(sources, (-y, x))
                changed = True

    # Revisit sources if they changed things
    if changed:
        heappush(sources, source)
        # print_state(state)
        # print(count_water(state), "waters")

print_state(state)
print(count_water(state), "waters")
